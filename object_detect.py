#!/usr/bin/env python
#
# This file is part of Smart Fridge App project.
# It is subject to the license terms in the COPYING.MIT file found in the
# top-level directory of this distribution

import os
import cv2
import sys
import numpy as np
from random import randint
import time
from datetime import datetime

import read_labelmap

# Define the home directory & the directory the app is installed in
home_dir = '/home/root'
app_dir = os.path.join(home_dir,'fridge-app')

# TODO: create a while loop waiting for closure of fridge door
# Image capture
cap = cv2.VideoCapture(0)
counter = 0;

# Capture frame
ret, frame = cap.read()

# When image capture done, release the capture
cap.release()
cv2.destroyAllWindows()

# Running the object detection algorithm
frozen_graph = os.path.join(home_dir,\
        'models/fridge-demo/ssd_mobilenet_v2_fridge_12022020/frozen_inference_graph.pb')
frozen_graph_pbtxt = os.path.join(home_dir,\
        'models/fridge-demo/ssd_mobilenet_v2_fridge_12022020/fine_tuned_graph.pbtxt')
tensorflowNet = cv2.dnn.readNetFromTensorflow(frozen_graph,frozen_graph_pbtxt)

# The image width and height are found to be the optimum for the screen used in
# the fridge demo
TOT_WIDTH = 1024
SIDE_WIDTH = 300
IMG_WIDTH = TOT_WIDTH - SIDE_WIDTH
IMG_HEIGHT = 656
img = cv2.resize(frame, (IMG_WIDTH,IMG_HEIGHT))
rows, cols, channels = img.shape

tensorflowNet.setInput(cv2.dnn.blobFromImage(img, size=(300, 300), \
        swapRB=True, crop=False))

# Time inference
start = time.time()
# Run inference
networkOutput = tensorflowNet.forward()
total = (time.time() - start) * 1000
inference_time = "inference time = {0:.0f}ms".format(total)

# Print inference time to terminal
print(inference_time)

# Text formatting
font = cv2.FONT_HERSHEY_SIMPLEX
font_size = 0.7

labelmap = os.path.join(app_dir,'labelmap/labelmap.json')
colours_def = os.path.join(app_dir,'labelmap/common_colours.json')

# Assign different random colours to different labels for easier identification
# of different objects
num_labels = read_labelmap.get_num_labels(labelmap)
num_set_colours = read_labelmap.get_num_colours(colours_def)
colour = []
labels = [0] * num_labels

if num_labels > num_set_colours:
    for i in range(num_labels):
        colour.append((randint(1,255),randint(1,255),randint(1,255)))
else:
    for i in range(num_labels):
        colour.append(read_labelmap.get_colour_from_idx(colours_def,i))

for detection in networkOutput[0,0]:

    score = float(detection[2])
    if score > 0.7:
        score = format(score * 100, '.1f')
        category_id = int(detection[1])
        category_name = read_labelmap.get_label_name(labelmap, category_id)
        text = str(category_name + ' ' + str(score) +'%')
        left = detection[3] * cols
        top = detection[4] * rows
        right = detection[5] * cols
        bottom = detection[6] * rows

        # Check that the detection points are not beyond the boundaries of the
        # image, and limit them at the boundary if that's the case
        if left < 0:
            left = 0
        if top < 0:
            top = 0
        if right > IMG_WIDTH:
            right = IMG_WIDTH
        if bottom > IMG_HEIGHT:
            bottom = IMG_HEIGHT

        # For placing the image label on the top left of the box
        left_label = int(left)+3
        top_label = int(top)-10
        if top_label < 20:
            top_label = int(top)+15
        top_left=(left_label,top_label)

        cv2.rectangle(img,(int(left),int(top)),(int(right),int(bottom)),\
                colour[category_id - 1],thickness=3)
        cv2.putText(img,text,top_left,font,font_size,colour[category_id - 1],\
                2,cv2.LINE_AA)
        labels[category_id-1] = labels[category_id-1] + 1

# Print contents on right hand side of the image
canvas = np.ones((656, 300, 3), dtype = "uint8") * 255
img = cv2.hconcat([img,canvas])
label_height = 50;

for i in range(0,num_labels):
    if labels[i] != 0:
        category_name = read_labelmap.get_label_name(labelmap,i+1)
        num_items = str(labels[i]) + "x"
        cv2.putText(img,num_items,(IMG_WIDTH+10,label_height),font,font_size,(0,0,0),2,cv2.LINE_AA)
        cv2.putText(img,category_name,(IMG_WIDTH+40,label_height),font,font_size,(0,0,0),2,cv2.LINE_AA)
        label_height = label_height + 50

# Overlay inference time measurement to bottom right of the image in black
cv2.putText(img,inference_time,(IMG_WIDTH+10,IMG_HEIGHT-35),\
        font,font_size,(0,0,0),2,cv2.LINE_AA)

# Overlay date and time on the bottom left of the image in black
now = datetime.today()
date_time = now.strftime("%d-%m-%Y %H:%M:%S")
cv2.putText(img,date_time,(5,IMG_HEIGHT-35),font,font_size,(0,0,0),2,cv2.LINE_AA)

# Save output images in images directory. Images named according to the date and
# time
date_time = now.strftime("%d-%m-%Y-%H%M%S")
img_name = "fridge_contents_{}.jpg".format(date_time)
target_dir = os.path.join(app_dir,'images')
if not os.path.exists(target_dir):
    os.makedirs(target_dir)
output_img = os.path.join(target_dir,img_name)
cv2.imwrite(output_img,img)

# Display the resulting image and close application when image is closed
cv2.imshow('Fridge Contents',img)
while True:
    key = cv2.waitKey(50)
    if cv2.getWindowProperty('Fridge Contents', cv2.WND_PROP_VISIBLE) < 1:
        cv2.destroyAllWindows()
        break

