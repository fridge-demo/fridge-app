#!/bin/bash
#
# This file is part of Smart Fridge App project.
# It is subject to the license terms in the COPYING.MIT file found in the
# top-level directory of this distribution

# Usage:
# 	./pbtxt_to_json.sh <path to pbtxt labelmap>
#
#	This will create a file labelmap.json in the current directory
#

INPUT_FILE=$1
OUTPUT_FILE="labelmap.json"

if [ -z ${INPUT_FILE} ];then
	echo "Error: No input file specified!"
	exit 1
fi

i=2	# The first identity is only 3 lines in
label_counter=1

echo "[" > "${OUTPUT_FILE}"

while IFS= read -r line
do
	let i+=1
	if [ $i -eq 5 ];then
		label="$(echo "${line}" | awk -F"'" '{print $2}')"
		cat >>"${OUTPUT_FILE}"<<-EOF
		   {
		   "name": "NULL",
		   "id": ${label_counter},
		   "display_name": "${label}"
		},
		EOF
		i=0
		let label_counter+=1
	fi
done < "${INPUT_FILE}"

NEW_FILE="$(sed '$d' "${OUTPUT_FILE}")"
echo "${NEW_FILE}" > "${OUTPUT_FILE}"
echo "}]" >> "${OUTPUT_FILE}"
