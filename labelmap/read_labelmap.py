#!/usr/bin/env python
#
# This file is part of Smart Fridge App project.
# It is subject to the license terms in the COPYING.MIT file found in the
# top-level directory of this distribution

import json

def get_label_name(path_to_labelmap, category_id):
    """ Gets label name from category id input

    Args:
        path_to_labelmap: The labelmap filepath
        category_id: The category_id value of the item. This must be an integer.

    Returns:
        display_name: The display_name string of the dictionary in position
            category_id.
    """
    labelmap = open(path_to_labelmap, "r")

    contents = labelmap.read()

    dict_list = json.loads(contents)
    dictionary = dict_list[category_id - 1]
    display_name = dictionary["display_name"]
    return(display_name)

def get_num_labels(path_to_labelmap):
    """ Gets the number of labels in the json labelmap

    Args:
        path_to_labelmap: The labelmap filepath

    Returns:
        num_labels: The number of labels/fields present in the labelmap
    """
    labelmap = open(path_to_labelmap, "r")
    contents = labelmap.read()

    dict_list = json.loads(contents)

    # Calculate number of labels
    num_labels = len(dict_list)

    return(num_labels)

def get_colour_from_idx(path_to_colours_dict, idx):
    """ Returns the BGR value of a colour for a specific idx in the colours
    dictionary

    Args:
        path_to_colours_dict: The path to the json colour definitions file
        idx: the id of a colour in the colours dictionary

    Returns:
        colour_bgr: the bgr value of a specific coulour in the colours
            dictionary
    """
    if idx > get_num_colours(path_to_colours_dict):
        return -1

    colours_dict = open(path_to_colours_dict, "r")
    contents = colours_dict.read()

    colours_list = json.loads(contents)
    dictionary = colours_list[idx]
    colour_bgr = dictionary["BGR"]
    return(colour_bgr)


def get_num_colours(path_to_colours_dict):
    """ Gets the number of colours in the json colours dictionary

    Args:
        path_to_colours_dict: The path to the json colour definitions file

    Returns:
        num_colours: The number of colours defined in that file
    """
    colours_dict = open(path_to_colours_dict, "r")
    contents = colours_dict.read()

    colours_list = json.loads(contents)

    # Calculate number of colours in the common colours dictionary.
    num_colours = len(colours_list)

    return num_colours

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Read a json labelmap')
    parser.add_argument('--input', required=True, help='path to labelmap')
    parser.add_argument('--catID', required=True, type=int, help='category id\
            that you would like to test.')
    args = parser.parse_args()

    print("The labelmap " + args.input + " contains: " + \
            str(get_num_labels(args.input)) + " labels")
    print("The label in position " + str(args.catID) + " is: " + \
            get_label_name(args.input, args.catID))
