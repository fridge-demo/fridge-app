# Fridge app

This repository is for any files to do with the final fridge demo application.

## Dependencies

Python >= 2.7.13
OpenCV >= 4.1.1
Python-numpy >= 1.13.1

## Setup
Run the following command on the target from the base directory (fridge-app)
```
export PYTHONPATH=`pwd`/labelmap
```

If you have a pbtxt labelmap, you will need to convert this into a JSON
labelmap. You can do this by copying your labelmap into the labelmap/ directory,
then:
```
cd labelmap/
./pbtxt_to_json.sh <labelmap path>
```

The application is written such that when it is built using the [meta-fridge-obj-detect-demo](https://gitlab.com/fridge-demo/meta-fridge-obj-detect-demo)
it will work out of the box. If you wish to use your own Tensorflow models, you
will need to update the frozen\_infererence\_graph.pb, fine\_tuned\_model.pbtxt,
and labelmap file paths in the object\_detect.py script.

--------------------------------------------------------------------------------
This project is licensed under the terms of the MIT license (please see
COPYING.MIT for further details).
